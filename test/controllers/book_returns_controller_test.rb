require 'test_helper'

class BookReturnsControllerTest < ActionController::TestCase
  setup do
    @book_return = book_returns(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:book_returns)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create book_return" do
    assert_difference('BookReturn.count') do
      post :create, book_return: { bookName: @book_return.bookName, bookNumber: @book_return.bookNumber, dateIssued: @book_return.dateIssued, dateReturn: @book_return.dateReturn, dueDate: @book_return.dueDate, firstName: @book_return.firstName, lastName: @book_return.lastName, student_id_no: @book_return.student_id_no }
    end

    assert_redirected_to book_return_path(assigns(:book_return))
  end

  test "should show book_return" do
    get :show, id: @book_return
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @book_return
    assert_response :success
  end

  test "should update book_return" do
    patch :update, id: @book_return, book_return: { bookName: @book_return.bookName, bookNumber: @book_return.bookNumber, dateIssued: @book_return.dateIssued, dateReturn: @book_return.dateReturn, dueDate: @book_return.dueDate, firstName: @book_return.firstName, lastName: @book_return.lastName, student_id_no: @book_return.student_id_no }
    assert_redirected_to book_return_path(assigns(:book_return))
  end

  test "should destroy book_return" do
    assert_difference('BookReturn.count', -1) do
      delete :destroy, id: @book_return
    end

    assert_redirected_to book_returns_path
  end
end
