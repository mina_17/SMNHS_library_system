require 'test_helper'

class BookIssuesControllerTest < ActionController::TestCase
  setup do
    @book_issue = book_issues(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:book_issues)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create book_issue" do
    assert_difference('BookIssue.count') do
      post :create, book_issue: { bookName: @book_issue.bookName, bookNumber: @book_issue.bookNumber, dateIssued: @book_issue.dateIssued, firstName: @book_issue.firstName, lastName: @book_issue.lastName, studentIdNo: @book_issue.studentIdNo }
    end

    assert_redirected_to book_issue_path(assigns(:book_issue))
  end

  test "should show book_issue" do
    get :show, id: @book_issue
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @book_issue
    assert_response :success
  end

  test "should update book_issue" do
    patch :update, id: @book_issue, book_issue: { bookName: @book_issue.bookName, bookNumber: @book_issue.bookNumber, dateIssued: @book_issue.dateIssued, firstName: @book_issue.firstName, lastName: @book_issue.lastName, studentIdNo: @book_issue.studentIdNo }
    assert_redirected_to book_issue_path(assigns(:book_issue))
  end

  test "should destroy book_issue" do
    assert_difference('BookIssue.count', -1) do
      delete :destroy, id: @book_issue
    end

    assert_redirected_to book_issues_path
  end
end
