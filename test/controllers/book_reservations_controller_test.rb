require 'test_helper'

class BookReservationsControllerTest < ActionController::TestCase
  setup do
    @book_reservation = book_reservations(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:book_reservations)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create book_reservation" do
    assert_difference('BookReservation.count') do
      post :create, book_reservation: { bookName: @book_reservation.bookName, bookNumber: @book_reservation.bookNumber, dateReservation: @book_reservation.dateReservation, firstName: @book_reservation.firstName, lastName: @book_reservation.lastName, student_id_no: @book_reservation.student_id_no }
    end

    assert_redirected_to book_reservation_path(assigns(:book_reservation))
  end

  test "should show book_reservation" do
    get :show, id: @book_reservation
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @book_reservation
    assert_response :success
  end

  test "should update book_reservation" do
    patch :update, id: @book_reservation, book_reservation: { bookName: @book_reservation.bookName, bookNumber: @book_reservation.bookNumber, dateReservation: @book_reservation.dateReservation, firstName: @book_reservation.firstName, lastName: @book_reservation.lastName, student_id_no: @book_reservation.student_id_no }
    assert_redirected_to book_reservation_path(assigns(:book_reservation))
  end

  test "should destroy book_reservation" do
    assert_difference('BookReservation.count', -1) do
      delete :destroy, id: @book_reservation
    end

    assert_redirected_to book_reservations_path
  end
end
