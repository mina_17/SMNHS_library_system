# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

User.create(lastname: 'Estrael', firstname: 'Riza Angelica', middlename: 'Niegos', email: 'ranestrael@gmail.com', password: 'riznel11', role: 'student')
User.create(lastname: 'Trator', firstname: 'Ad', middlename: 'Minis', email: 'admin@gmail.com', password: '12345678', role: 'admin')
User.create(lastname: 'Dent', firstname: 'Ist', middlename: 'Tu', email: 'student@gmail.com', password: '12345678', role: 'student')
