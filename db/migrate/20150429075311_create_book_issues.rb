class CreateBookIssues < ActiveRecord::Migration
  def change
    create_table :book_issues do |t|
      t.string :bookName
      t.string :bookNumber
      t.string :firstName
      t.string :lastName
      t.string :studentIdNo
      t.string :dateIssued

      t.timestamps null: false
    end
  end
end
