class CreateReturns < ActiveRecord::Migration
  def change
    create_table :returns do |t|
      t.string :index

      t.timestamps null: false
    end
  end
end
