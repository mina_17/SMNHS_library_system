class CreateBookReturns < ActiveRecord::Migration
  def change
    create_table :book_returns do |t|
      t.string :bookName
      t.string :bookNumber
      t.string :firstName
      t.string :lastName
      t.string :student_id_no
      t.string :dateReturn
      t.string :dateIssued
      t.string :dueDate

      t.timestamps null: false
    end
  end
end
