class CreateBookReservations < ActiveRecord::Migration
  def change
    create_table :book_reservations do |t|
      t.string :bookName
      t.string :bookNumber
      t.string :firstName
      t.string :lastName
      t.string :middlename
      t.string :student_id_no
      t.string :dateReservation

      t.timestamps null: false
    end
  end
end
