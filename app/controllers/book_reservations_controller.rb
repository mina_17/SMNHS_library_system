class BookReservationsController < ApplicationController
  before_action :set_book_reservation, only: [:show, :edit, :update, :destroy]

  # GET /book_reservations
  # GET /book_reservations.json
  def index
    @book_reservations = BookReservation.all
  end

  # GET /book_reservations/1
  # GET /book_reservations/1.json
  def show
  end

  # GET /book_reservations/new
  def new
    @book_reservation = BookReservation.new
  end

  # GET /book_reservations/1/edit
  def edit
  end

  # POST /book_reservations
  # POST /book_reservations.json
  def create
    @book_reservation = BookReservation.new(book_reservation_params)

    respond_to do |format|
      if @book_reservation.save
        format.html { redirect_to @book_reservation, notice: 'Book reservation was successfully created.' }
        format.json { render :show, status: :created, location: @book_reservation }
      else
        format.html { render :new }
        format.json { render json: @book_reservation.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /book_reservations/1
  # PATCH/PUT /book_reservations/1.json
  def update
    respond_to do |format|
      if @book_reservation.update(book_reservation_params)
        format.html { redirect_to @book_reservation, notice: 'Book reservation was successfully updated.' }
        format.json { render :show, status: :ok, location: @book_reservation }
      else
        format.html { render :edit }
        format.json { render json: @book_reservation.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /book_reservations/1
  # DELETE /book_reservations/1.json
  def destroy
    @book_reservation.destroy
    respond_to do |format|
      format.html { redirect_to book_reservations_url, notice: 'Book reservation was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_book_reservation
      @book_reservation = BookReservation.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def book_reservation_params
      params.require(:book_reservation).permit(:bookName, :firstName, :middlename, :lastName, :student_id_no, :dateReservation)
    end
end
