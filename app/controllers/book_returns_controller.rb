class BookReturnsController < ApplicationController
  before_action :set_book_return, only: [:show, :edit, :update, :destroy]

  # GET /book_returns
  # GET /book_returns.json
  def index
    @book_returns = BookReturn.all
  end

  # GET /book_returns/1
  # GET /book_returns/1.json
  def show
  end

  # GET /book_returns/new
  def new
    @book_return = BookReturn.new
  end

  # GET /book_returns/1/edit
  def edit
  end

  # POST /book_returns
  # POST /book_returns.json
  def create
    @book_return = BookReturn.new(book_return_params)

    respond_to do |format|
      if @book_return.save
        format.html { redirect_to @book_return, notice: 'Book return was successfully created.' }
        format.json { render :show, status: :created, location: @book_return }
      else
        format.html { render :new }
        format.json { render json: @book_return.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /book_returns/1
  # PATCH/PUT /book_returns/1.json
  def update
    respond_to do |format|
      if @book_return.update(book_return_params)
        format.html { redirect_to @book_return, notice: 'Book return was successfully updated.' }
        format.json { render :show, status: :ok, location: @book_return }
      else
        format.html { render :edit }
        format.json { render json: @book_return.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /book_returns/1
  # DELETE /book_returns/1.json
  def destroy
    @book_return.destroy
    respond_to do |format|
      format.html { redirect_to book_returns_url, notice: 'Book return was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_book_return
      @book_return = BookReturn.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def book_return_params
      params.require(:book_return).permit(:bookName, :bookNumber, :firstName, :lastName, :student_id_no, :dateReturn, :dateIssued, :dueDate)
    end
end
