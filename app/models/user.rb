class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  # private
  # def user_params
  #   params.require(:user).permit(:lastname, :firstname, :middlename)
  # end

end
