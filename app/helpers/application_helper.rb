module ApplicationHelper
  def admin?
    signed_in? && current_user.role == 'admin'
  end

  def student?
    signed_in? && current_user.role == 'student'
  end
end
