json.array!(@book_returns) do |book_return|
  json.extract! book_return, :id, :bookName, :bookNumber, :firstName, :lastName, :student_id_no, :dateReturn, :dateIssued, :dueDate
  json.url book_return_url(book_return, format: :json)
end
