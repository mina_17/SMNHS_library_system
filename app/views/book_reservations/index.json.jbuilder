json.array!(@book_reservations) do |book_reservation|
  json.extract! book_reservation, :id, :bookName, :bookNumber, :firstName, :lastName, :student_id_no, :dateReservation
  json.url book_reservation_url(book_reservation, format: :json)
end
