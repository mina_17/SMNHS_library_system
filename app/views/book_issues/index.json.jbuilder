json.array!(@book_issues) do |book_issue|
  json.extract! book_issue, :id, :bookName, :bookNumber, :firstName, :lastName, :studentIdNo, :dateIssued
  json.url book_issue_url(book_issue, format: :json)
end
